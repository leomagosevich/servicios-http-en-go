package main

import (
	"encoding/json"
	"log"
	"net/http"
	"github.com/gorilla/mux"
)

type Person struct{
	ID string `json:"id,omitempty"`
	FirstName string `json:"firstname,omitempty"`
	LastName string `json:"lastname,omitempty"`
	Address *Address `json:"address,omitempty"`
}

type Address struct{
	City string `json:"city,omitempty"`
	State string `json:"state,omitempty"`
}

var people []Person

//Lógica de cada endpoint
func GetPeopleEndpoint(w http.ResponseWriter, req *http.Request){
	json.NewEncoder(w).Encode(people)
}

func GetPersonEndpoint(w http.ResponseWriter, req *http.Request){
	params := mux.Vars(req)
	
	for _, item := range people {
		if item.ID == params["id"]{
			json.NewEncoder(w).Encode(item)
			return
		}
	}
	
	json.NewEncoder(w).Encode(&Person{})
}

func CreatePersonEndpoint(w http.ResponseWriter, req *http.Request){
	params := mux.Vars(req)
	var person Person

	_ = json.NewDecoder(req.Body).Decode(&person)
	person.ID = params["id"]
	people = append(people, person)
	
	json.NewEncoder(w).Encode(people)
}

func DeletePersonEndpoint(w http.ResponseWriter, req *http.Request){
	params := mux.Vars(req)

	for index, item := range people {
		if item.ID == params["id"] {
			people = append(people[:index], people[index + 1:]...)
			break
		}
	}

	json.NewEncoder(w).Encode(people)
}

func main(){
	// declaro e instancio el router
	router := mux.NewRouter()

	// Inicializo el array people
	people = append(people, Person{ID: "1", FirstName: "Juan", LastName: "Pérez", Address: &Address{City: "Quilmes", State: "Buenos Aires"}})
	people = append(people, Person{ID: "2", FirstName: "Tito", LastName: "Puente", Address: &Address{City: "La Falda", State: "Córdoba"}})

	// rutas - endpoints
	router.HandleFunc("/getPeople", GetPeopleEndpoint).Methods("GET")
	router.HandleFunc("/getPerson&{id}", GetPersonEndpoint).Methods("GET")
	router.HandleFunc("/getPerson&{id}", CreatePersonEndpoint).Methods("POST")
	router.HandleFunc("/getPerson&{id}", DeletePersonEndpoint).Methods("DELETE")

	// listener
	//http.ListenAndServe(":8080", router)

	log.Fatal(http.ListenAndServe(":8080", router))

}