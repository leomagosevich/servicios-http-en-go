package main

import (
	"math/rand"
	"time"
	"encoding/json"
	"log"
	"net/http"
	"github.com/gorilla/mux"
)

//Estructura que emula los parámetros a retornar por el sensor de temperatura
type Temp struct {
	Temperatura float32 `json:"temperature"`
	Humedad float32 `json:"humidity"`
}

//Función Random para generar valores de humedad y temperatura aleatorios a retornar
func temperatura() Temp {
	var multit int
	var multih int
	var multifh float32
	var multift float32
	var humedad float32
	var temperatura float32

	//Semilla aleatoria para que el random varie entre ejecución y ejecución
	rand.Seed(time.Now().Unix())

	//Se definen multiplicadores para que existan valores aleatorios mayores a la unidad
	multit = int(rand.Int31n(50))
	multih = int(rand.Int31n(100))
	//Para que no se produzca error al multiplicar los random generados, se transforman los multiplicadores a flotantes
	multift = float32(multit)
	multifh = float32(multih)

	//Artilugio para que se generen valores negativos
	if multift < 25 {
		multift = multift * (-1)
	}

	//temperatura = float32(rand.Float32() * multift)
	temperatura = rand.Float32() * multift

	switch {
    case temperatura == -0:
        temperatura = -0.01
    case temperatura == 0:
        temperatura = 0.01
    }
	
	humedad = rand.Float32() * multifh

	//Artilugio para generar valores de humedad que que se asemejen a datos reales
	switch {
    case humedad < 30:
        humedad = humedad + 50
    case humedad > 100:
        humedad = humedad - 50
    default:
        humedad = humedad + 1
    }

	// Se define la variable temp como del tipo Temp (estructura) e inicializa la misma con los valores aleatorios generados
	temp := Temp{Temperatura: temperatura, Humedad: humedad}

	return temp
}

//Función endpoint que se encarga de retornar un json ante un GET, para ello invoca a la función temperatura
func GetTempEndpoint(w http.ResponseWriter, req *http.Request){
	temp := temperatura()
	json.NewEncoder(w).Encode(temp)
}

func main() {
	//Se declara e instancia el router
	router := mux.NewRouter()

	//Se define una ruta - endpoint
	//La URL será http://192.168.3.125:8080/getTemp
	router.HandleFunc("/getTemp", GetTempEndpoint).Methods("GET")

	//Listener - escucha en el puerto 8080
	log.Fatal(http.ListenAndServe(":8080", router))

}